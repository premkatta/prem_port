import React, { Component } from 'react';
import logo from './logo.svg';
import {BrowserRouter, Route} from "react-router-dom";
import {NavLink} from "react-router-dom";
import './css/App.css';
import About from './components/about';
import Skills from './components/skills';
import Projects from './components/projects';
import Contact from './components/contact';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <div className="App">
        <div class = "leftpanel">
        <div class = "leftpanelText">
       
        <NavLink to = "/" class = " tabText tab_hOver"> <div class = "nameText">
        PREM KATTA
        </div></NavLink>
        <div class = "tabText" >
        <NavLink to = "/about" class = " tabText tab_hOver"> <p >ABOUT</p></NavLink>
        <NavLink to = "/skills"  class = "tabText tab_hOver"> <p >SKILLS</p></NavLink>
        <NavLink to = "/projects" class = " tabText tab_hOver"> <p >PROJECTS</p></NavLink>
        <NavLink to = "/contact" class = " tabText tab_hOver"> <p >CONTACT</p></NavLink>       
        </div>
        <div class = "icons">
        <div>
        <img src = {require('./images/linkedin2.svg')}></img>
        <a href = "https://www.linkedin.com"><p class = "inline-block"> LINKED IN</p> </a>
        </div>
        <div>
        <img src = {require('./images/github.svg')}></img>
        <a href = "https://gitlab.com/dashboard/projects"><p class = "inline-block"> GITHUB</p></a>
        </div>
        <div>
        <img src = {require('./images/facebook.svg')}></img>
       <a href = "https://www.facebook.com"><p class = "inline-block"> FACEBOOK</p> </a> 
        </div>
        <div>
        <img src = {require('./images/twitter.svg')}></img>
       <a href = "https://twitter.com"><p class = "inline-block"> TWITTER</p></a> 
        </div>
        
        </div>
        </div> 
        </div>
        <Route path = "/"  exact component = {About}> </Route>
        <Route path = "/about"  exact component = {About}> </Route>
        <Route path = "/skills"  exact component = {Skills}> </Route>
        <Route path = "/projects"  exact component = {Projects}> </Route>
        <Route path = "/contact"  exact component = {Contact}> </Route>
      </div>
      </BrowserRouter>
    );
  }
}

export default App;

import React , {Component} from "react";

class Skills extends Component{
    constructor(props){
        super()
    }

   
render(){
    return(
        <div class = "skills">
            <p>HTML</p>
            
            <div class="progress">
                <div class="progress-bar" role="progressbar" style={{width: '85%'}}aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p>CSS3</p>
            
            <div class="progress">
                <div class="progress-bar" role="progressbar" style={{width: '65%'}}aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p>JavaScript</p>
            
            <div class="progress">
                <div class="progress-bar" role="progressbar" style={{width: '77%'}}aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p>ReactJs</p>
            
            <div class="progress">
                <div class="progress-bar" role="progressbar" style={{width: '85%'}}aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p>MongoDb</p>
            
            <div class="progress">
                <div class="progress-bar" role="progressbar" style={{width: '59%'}}aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p>SQL</p>
            
            <div class="progress">
                <div class="progress-bar" role="progressbar" style={{width: '75%'}}aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>

        </div>
    )

};

}

export default Skills;
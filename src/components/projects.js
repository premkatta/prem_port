import React , {Component} from "react";


class Projects extends Component{
    constructor(props){
        super()
    }

render(){
    return(
        <div class = "projects">
        <div class="card-group">
  <div class="card">
    <img class="card-img-top laptop" src="https://s3-alpha.figma.com/img/388f/4da2/869f440accd4071d26759362df0b52a5" alt="Card image cap"></img>
    <div class="card-body">
      <h5 class="card-title">WELOVEDAILY</h5>
      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>

      <h5> DESCRIPTION</h5>
      <p class="card-text">This card has supporting  text below as a natural lead-in to additional content text below as a natural lead-in to additional content.</p>

      <h5>STACK</h5>
      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>

      <div class = "code_div">
        < img class = "code_logo" src ='https://camo.githubusercontent.com/7710b43d0476b6f6d4b4b2865e35c108f69991f3/68747470733a2f2f7777772e69636f6e66696e6465722e636f6d2f646174612f69636f6e732f6f637469636f6e732f313032342f6d61726b2d6769746875622d3235362e706e67'></img>
          <a href = "https://gitlab.com/premkatta/welovedaily2"><p class = "inline-block code">VIEW CODE IN GITHUB</p></a> 
      </div>

    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="https://s3-alpha.figma.com/img/5909/a658/c0caf40aea2a1d434b6aa799b13a92ee" alt="Card image cap"></img>
    <div class="card-body">
      <h5 class="card-title">PROJECT NAME</h5>
      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
      
      <h5> DESCRIPTION</h5>
      <p class="card-text">This card has supporting  text below as a natural lead-in to additional content text below as a natural lead-in to additional content.</p>

      <h5>STACK</h5>
      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>

      <div class = "code_div">
        <img class = "code_logo" src ='https://camo.githubusercontent.com/7710b43d0476b6f6d4b4b2865e35c108f69991f3/68747470733a2f2f7777772e69636f6e66696e6465722e636f6d2f646174612f69636f6e732f6f637469636f6e732f313032342f6d61726b2d6769746875622d3235362e706e67'></img>
        <a href = "https://gitlab.com/premkatta/app1"><p class = "inline-block code" >VIEW CODE IN GITHUB</p> </a>
      </div>

    </div>
  </div>
  </div>
</div>
    )

};

}

export default Projects;